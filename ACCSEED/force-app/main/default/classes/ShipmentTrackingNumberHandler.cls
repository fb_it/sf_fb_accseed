public with sharing class ShipmentTrackingNumberHandler {

    @future(Callout=true)
    public static void sendTrackingNumber(String newListJSON) {
        List<Shipment_Tracking_Numbers__c> newList = (List<Shipment_Tracking_Numbers__c>) JSON.deserialize(newListJSON, List<Shipment_Tracking_Numbers__c>.class);

        List<Shipment_Tracking_Numbers__c> queryList = [Select id, Individual_Tracking_Number__c,Shipment__r.Magento_Shipment_ID__c from Shipment_Tracking_Numbers__c where ID in: newList];
        List<Shipment_Tracking_Numbers__c> stnToUpdate = new List<Shipment_Tracking_Numbers__c>();
        magento2.Port m = new magento2.Port();
        String sessionId = MagentoCalls.fetchSessionID();
        for(Shipment_Tracking_Numbers__c stn:newList)
        {

            Integer stnID = m.salesOrderShipmentAddTrack(sessionId,stn.Shipment__r.Magento_Shipment_ID__c,null,null,stn.Individual_Tracking_Number__c);

            stn.Magento_Tracking_Number_ID__c = stnID;
            stnToUpdate.add(stn);
        }

        update stnToUpdate;

    }
}