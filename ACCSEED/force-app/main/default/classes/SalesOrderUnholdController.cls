public with sharing class SalesOrderUnholdController {


    public  AcctSeedERP__Sales_Order__c so {get;set;}
    public SalesOrderUnholdController(ApexPages.StandardController sc){

        if(!Test.isRunningTest())
        {
            sc.addFields(new List<String>{'Previous_Status__c,AcctSeedERP__Status__c,AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c'});
        }

        so = (AcctSeedERP__Sales_Order__c) sc.getRecord();

    }

    public PageReference sendToMagento(){

        magento2.Port m = new magento2.Port();
        String sessionId = MagentoCalls.fetchSessionID();
        system.debug(sessionId);
        system.debug(so.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c);

        Integer response = m.salesOrderUnhold(sessionId,so.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c);
        system.debug(response);

        so.Previous_Status__c = null;
        so.AcctSeedERP__Status__c = so.Previous_Status__c;
        update so;

        PageReference pr = new PageReference('/'+so.id);
        pr.setRedirect(true);

        return pr;
    }

}