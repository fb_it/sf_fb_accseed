public with sharing class SalesOrderTriggerHandler {


    @future(Callout=true)
    public static void sendStatusUpdate(String newListJSON, String oldMapJSON)
    {
        List<AcctSeedERP__Sales_Order__c> newList = (List<AcctSeedERP__Sales_Order__c>)JSON.deserialize(newListJSON,List<AcctSeedERP__Sales_Order__c>.class);
        Map<id,AcctSeedERP__Sales_Order__c> oldMap = (Map<id,AcctSeedERP__Sales_Order__c>)JSON.deserialize(oldMapJSON,Map<id,AcctSeedERP__Sales_Order__c>.class);
        List<AcctSeedERP__Sales_Order__c> soToSend = new List<AcctSeedERP__Sales_Order__c>();

        for(AcctSeedERP__Sales_Order__c so:[Select id, AcctSeedERP__Status__c, AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c from AcctSeedERP__Sales_Order__c where id IN: newList])
        {
            system.debug('so.id -- ' + so.id);
            AcctSeedERP__Sales_Order__c oldSO = oldMap.get(so.id);
            system.debug('oldSO.AcctSeedERP__Status__c -- ' + oldSO.AcctSeedERP__Status__c);
            system.debug('so.AcctSeedERP__Status__c -- ' + so.AcctSeedERP__Status__c);
            if(so.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c!=null && so.AcctSeedERP__Status__c != oldSO.AcctSeedERP__Status__c)
            {
                if(oldSO.AcctSeedERP__Status__c == 'Processing' && so.AcctSeedERP__Status__c == 'Warehouse Processing')
                {
                    soToSend.add(so);
                }
                else if(oldSO.AcctSeedERP__Status__c != so.AcctSeedERP__Status__c && so.AcctSeedERP__Status__c == 'Processing')
                {
                    soToSend.add(so);
                }
                else if(oldSO.AcctSeedERP__Status__c == 'PO Pending Payment' && (so.AcctSeedERP__Status__c == 'Complete' || so.AcctSeedERP__Status__c == 'Partial' || so.AcctSeedERP__Status__c == 'Warehouse Processing'))
                {
                    soToSend.add(so);
                }
            }
        }

        if(!soToSend.isEmpty()) {
            magento2.Port m = new magento2.Port();
            String sessionId = MagentoCalls.fetchSessionID();
            for (AcctSeedERP__Sales_Order__c so : soToSend) {
                m.salesOrderAddComment(sessionId, so.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c, fetchAPIName(so.AcctSeedERP__Status__c), null, null);
            }
        }
    }

    @future(Callout=true)
    public static void createInvoice(String newListJSON, String oldMapJSON)
    {
        List<AcctSeedERP__Sales_Order__c> newList = (List<AcctSeedERP__Sales_Order__c>)JSON.deserialize(newListJSON,List<AcctSeedERP__Sales_Order__c>.class);
        Map<id,AcctSeedERP__Sales_Order__c> oldMap = (Map<id,AcctSeedERP__Sales_Order__c>)JSON.deserialize(oldMapJSON,Map<id,AcctSeedERP__Sales_Order__c>.class);
        List<AcctSeedERP__Sales_Order__c> soToSend = new List<AcctSeedERP__Sales_Order__c>();

        Set<id> OppProductIds = new Set<id>();
        for(AcctSeedERP__Sales_Order__c so:[Select id, AcctSeedERP__Status__c, AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c,AcctSeedERP__Opportunity__r.Customer_Email__c, (Select id,AcctSeedERP__Opportunity_Product_Id__c,AcctSeedERP__Quantity_Shipped__c from AcctSeedERP__Sales_Order_Line__r)  from AcctSeedERP__Sales_Order__c where id IN: newList])
        {
            AcctSeedERP__Sales_Order__c oldSO = oldMap.get(so.id);
            if(so.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c!=null && so.AcctSeedERP__Status__c != oldSO.AcctSeedERP__Status__c)
            {
                if(so.AcctSeedERP__Status__c == 'Paid' && so.AcctSeedERP__Status__c != oldSO.AcctSeedERP__Status__c)
                {
                    soToSend.add(so);

                    for(AcctSeedERP__Sales_Order_Line__c line:so.AcctSeedERP__Sales_Order_Line__r) {
                        OppProductIds.add(line.AcctSeedERP__Opportunity_Product_Id__c);
                    }
                }
            }
        }

        Map<id,OpportunityLineItem> oppLineMap = new Map<Id, OpportunityLineItem>([Select id, tnw_mage_basic__Magento_ID__c from OpportunityLineItem where ID in: OppProductIds]);
        if(!soToSend.isEmpty()) {
            magento2.Port m = new magento2.Port();
            String sessionId = MagentoCalls.fetchSessionID();
            for (AcctSeedERP__Sales_Order__c so : soToSend) {

                magento2.orderItemIdQtyArray oiArr = new magento2.orderItemIdQtyArray();
                List<magento2.orderItemIdQty> arr = new List<magento2.orderItemIdQty>();
                system.debug('oppLineMap.keyset -- ' + oppLineMap.keyset());

                for(AcctSeedERP__Sales_Order_Line__c soLine:so.AcctSeedERP__Sales_Order_Line__r)
                {
                    system.debug('soLine.AcctSeedERP__Opportunity_Product_Id__c -- ' + soLine.AcctSeedERP__Opportunity_Product_Id__c);
                    if(oppLineMap.containsKey(soLine.AcctSeedERP__Opportunity_Product_Id__c) && oppLineMap.get(soLine.AcctSeedERP__Opportunity_Product_Id__c).tnw_mage_basic__Magento_ID__c!=null) {
                        magento2.orderItemIdQty item = new magento2.orderItemIdQty();
                        item.qty = soLine.AcctSeedERP__Quantity_Shipped__c;
                        item.order_item_id = Integer.valueof(oppLineMap.get(soLine.AcctSeedERP__Opportunity_Product_Id__c).tnw_mage_basic__Magento_ID__c);
                        arr.add(item);
                    }
                }

                if(!arr.isEmpty()) {
                    oiArr.complexObjectArray = arr;
                    m.salesOrderInvoiceCreate(sessionId, so.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c,oiArr,null,so.AcctSeedERP__Opportunity__r.Customer_Email__c,'0');
                }
            }
        }

    }

    public static String fetchAPIName(String value)
    {
        Schema.DescribeFieldResult fieldResult = AcctSeedERP__Sales_Order__c.AcctSeedERP__Status__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();

        Map<String,String> valueToAPIName = new Map<String,String>();
        for( Schema.PicklistEntry v : values) {
            valueToAPIName.put(v.getLabel(),v.getValue());
        }

        return valueToAPIName.get(value);
    }

}