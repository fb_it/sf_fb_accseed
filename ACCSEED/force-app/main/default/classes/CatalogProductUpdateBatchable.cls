global class CatalogProductUpdateBatchable implements Database.Batchable<Integer>,Database.AllowsCallouts {
    String jsonRequest;
    
    global List<CatalogProductUpdateActions.CatalogProductUpdateRequest> productUpdateBatch;
    global Integer allProductsSize;
    global Decimal numberOfBatches;
    public Integer batchCounter =0;
    public Integer failureCounter=0;

        

    public CatalogProductUpdateBatchable(List<CatalogProductUpdateActions.CatalogProductUpdateRequest> productUpdateBatch, Integer allProductsSize) {
        this.productUpdateBatch=productUpdateBatch;
        this.allProductsSize=allProductsSize;
    }
    public CatalogProductUpdateBatchable(List<CatalogProductUpdateActions.CatalogProductUpdateRequest> productUpdateBatch) {
        this.productUpdateBatch=productUpdateBatch;
        
    }
    //This tells it how many Batch apex to run. 
    global Iterable<Integer> start(Database.BatchableContext context) {
        
        Integer[] values = new Integer[0];
        numberOfBatches=allProductsSize/productUpdateBatch.size();
        System.debug('number of batchess is '+numberOfBatches+ ' '+allProductsSize+' '+ productUpdateBatch.size());
        if(!Test.isRunningTest()){
            if(Math.mod(allProductsSize,productUpdateBatch.size())==0){
                System.debug('mod 0');
                while(values.size() < 4 ) values.add(values.size());
            }
            else{
                System.debug('not mod 0');
                while(values.size() < 4) values.add(values.size());
            }
        }
        else{

            while(values.size() < 1) values.add(values.size());
        }
        
        return values;
        
        //List<CatalogProductUpdateActions.CatalogProductUpdateRequest> productUpdateBatch= productUpdateBatch;
        //return productUpdateBatch;
        
    }
    global void execute(Database.BatchableContext context, Integer[] values) {
                System.debug(values);
                magentoSOAP.Port m = new magentoSOAP.Port();
                System.debug('Reached port '+m);
                String sessionId = MagentoCalls.fetchSessionID();
                System.debug('Fetched sessionId!! : '+sessionId);
                magentoSOAP.catalogProductCreateEntity productData = new magentoSOAP.catalogProductCreateEntity();
                System.debug('productData: '+productData);

                for(CatalogProductUpdateActions.CatalogProductUpdateRequest request: productUpdateBatch){  
                    String jsonRequest= JSON.serialize(request);
                    //jsonRequest=jsonRequest.substring(1,jsonRequest.length()-1);
                    system.debug('jsonRequest -- ' + jsonRequest);
                    CatalogProductUpdateActions.CatalogProductUpdateRequest product = (CatalogProductUpdateActions.CatalogProductUpdateRequest)JSON.deserialize(jsonRequest,CatalogProductUpdateActions.CatalogProductUpdateRequest.class);
                    
                    String productId = product.ProductId;
                    String store = product.Store;
                    String identifierType = 'id';//product.IdentifierType;
                    String msrp= String.valueof(product.MSRP.setScale(2));
                    System.debug('MSRP= '+msrp);

                    productData.price = String.valueof(product.price.setScale(2));
                    System.debug(productData.price);
                    try{
                        if(Test.isRunningTest()){
                            Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdate'));
                        }
                        Boolean productUpdateResponse = m.catalogProductUpdate(sessionId,productId,productData,store,identifierType);
                            if(Test.isRunningTest()){
                                Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('msrp'));
                            }
                            Integer msrpResponse = m.catalogProductSetSingleData(sessionId,productId,'msrp',msrp,store);
                    }
                    catch(CalloutException ce){
                        CalloutException e = new CalloutException();
                        e.setMessage(productId+' '+ce);
                        throw e;
                    }
                    } 
            
    }
    global void finish(Database.BatchableContext context) {
        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,     JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:context.getJobId()];
        batchCounter++;
        system.debug(batchCounter);
        if (a.NumberOfErrors==a.TotalJobItems) {
            failureCounter++;
            /*
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Magento Update Batch Failed for a Product ');
            mail.setPlainTextBody(a.TotalJobItems+' records processed ' +
           'with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); */          
        }
        if(batchCounter==allProductsSize){
            /*
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Magento Update Batch Completed ');
            mail.setPlainTextBody('There were '+failureCounter+' number of Products that failed to update.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); */
        }
        
    }
}