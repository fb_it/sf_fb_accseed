public with sharing class OutboundInventoryMovTriggerHandler {

    @future(Callout=true)
    public static void SendToMagento(String newListJSON, Boolean isDelete)
    {
        List<AcctSeedERP__Outbound_Inventory_Movement__c> newList = (List<AcctSeedERP__Outbound_Inventory_Movement__c>)JSON.deserialize(newListJSON,List<AcctSeedERP__Outbound_Inventory_Movement__c>.class);
        magento2.Port m = new magento2.Port();
        String sessionId = MagentoCalls.fetchSessionID();

        system.debug(sessionId);

        for(AcctSeedERP__Outbound_Inventory_Movement__c oim:[Select id, AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c, AcctSeedERP__Quantity__c, AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Location__r.Magento_Sync__c from AcctSeedERP__Outbound_Inventory_Movement__c where ID in: newList AND AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Location__r.Magento_Sync__c = TRUE AND AcctSeedERP__Quantity__c >0 ALL ROWS])
        {
            Decimal quantity = isDelete ? oim.AcctSeedERP__Quantity__c : oim.AcctSeedERP__Quantity__c  *-1;
            system.debug('Quantity -- ' + String.valueof(quantity));
            //m.catalogInventoryStockItemUpdateDelta(sessionId,oim.AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c,(Integer)quantity);
        }


    }

}