public with sharing class SalesOrderCancelController {

    public  AcctSeedERP__Sales_Order__c so {get;set;}
    public SalesOrderCancelController(ApexPages.StandardController sc){

        if(!Test.isRunningTest())
        {
            sc.addFields(new List<String>{'AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c'});
        }

        so = (AcctSeedERP__Sales_Order__c) sc.getRecord();

    }

    public PageReference sendCancelToMagento(){

        magento2.Port m = new magento2.Port();
        String sessionId = MagentoCalls.fetchSessionID();
        system.debug(sessionId);
        system.debug(so.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c);

        try {
            Integer response = m.salesOrderCancel(sessionId, so.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c);
            system.debug(response);
        }
        catch(exception ex)
        {
            system.debug(ex.getMessage());
        }

        so.AcctSeedERP__Status__c = 'Canceled';
        update so;

        PageReference pr = new PageReference('/'+so.id);
        pr.setRedirect(true);

        return pr;
    }
}