global without sharing class CalculateDistancesController {

    private static final string CREATE_CD = 'create';
    private static final string DELETE_CD = 'delete';
    
    public void calculateDistances(){
        String campaignId = ApexPages.currentPage().getParameters().get('campaignId');
        
        System.debug('Starting batch for Campaign: ' + campaignId);
        
        //Always delete before inserting new. Delete batch will call deleteCompleted when completed.
        DeleteCalculatedDistances calDis = new DeleteCalculatedDistances(campaignId, CREATE_CD);
		Database.executeBatch(calDis,9000);
        
        System.debug('Batch started for Campaign: ' + campaignId);
    }
    
    public void deleteExistingDistances(){
        String campaignId = ApexPages.currentPage().getParameters().get('campaignId');
        
        System.debug('Starting delete batch for Campaign: ' + campaignId);
        
        DeleteCalculatedDistances calDis = new DeleteCalculatedDistances(campaignId, DELETE_CD);
		Database.executeBatch(calDis, 9000);
        
        System.debug('Delete Batch started for Campaign: ' + campaignId);
    } 
    
    public void deleteCompleted(String source, String campaignId){
        
        Campaign cmpObj = [Select Id, Maximum_Miles_from_POI__c, Maximum_POIs_per_Member__c, Name from Campaign where Id = :campaignId ];
        
        if(source == CREATE_CD){
            CalculateDistances calDis = new CalculateDistances(campaignId, Integer.valueOf(cmpObj.Maximum_POIs_per_Member__c), cmpObj.Maximum_Miles_from_POI__c);
            Database.executeBatch(calDis);            
        }
        
        String subject = 'Delete Distances Batch Processing Finished for Campaign : ' + cmpObj.Name;
        sendEmail(cmpObj, subject);          
    }
    
    
    public void distanceCalculationComplete(String campaignId){
        Campaign cmpObj = [Select Id, Calculate_Distances_Completed__c, Name from Campaign where Id = :campaignId ];
        cmpObj.Calculate_Distances_Completed__c = Datetime.now();
        
        update cmpObj;
        
        String subject = 'Calculate Distances Batch Processing Finished for Campaign : ' + cmpObj.Name;
        sendEmail(cmpObj, subject);  
    } 

    private void sendEmail(Campaign cmpObj, String subject){
        String campaignId = cmpObj.id;
        String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + campaignId;
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        
        User u = [select Email from user where id=:userinfo.getuserid()];
        
        List<String> toAddresses = new List<String>();
        toAddresses.add(u.Email);
        
        email.setSubject(subject);
        email.setToAddresses(toAddresses);
        
        email.setHtmlBody('Campaign URL : ' + fullRecordURL);
        
        System.debug('fullRecordURL: ' + fullRecordURL);
        
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        
    }    
}