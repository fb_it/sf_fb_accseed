public without sharing class PurchaseOrderLineQueueable implements Queueable{
    public List<AcctSeedERP__Purchase_Order_Line__c> polUpdates;

    public PurchaseOrderLineQueueable(List<AcctSeedERP__Purchase_Order_Line__c> polUpdates) {
        this.polUpdates=polUpdates;
        System.debug(this.polUpdates);
    }
    public PurchaseOrderLineQueueable() {
        this.polUpdates=polUpdates;
    }
    public void execute (QueueableContext context){
        List<AcctSeedERP__Purchase_Order_Line__c> polUpdateBatch= new List<AcctSeedERP__Purchase_Order_Line__c>();
        System.debug('polUpdateBatch is '+polUpdateBatch);
        Integer requestCounter=0;
        Integer batchSize=25;
        for(AcctSeedERP__Purchase_Order_Line__c pol: polUpdates){     
           polUpdateBatch.add(pol);
           //TODO: make batch size also invocable, && (!(requestCounter<(productVals.size()-Math.mod(productVals.size(),3
           if ((requestCounter+batchSize)>polUpdates.size()) {
                System.debug('Sending '+polUpdateBatch+' to the Batchable');
                Database.executeBatch(new POLBatchable(polUpdateBatch,polUpdates.size()), 1);
                requestCounter=requestCounter+polUpdateBatch.size();
                System.debug('Special request counter '+requestCounter);
                polUpdateBatch.clear();
           } 
           //else if (polUpdateBatch.size() == batchSize ) {
            else{
                System.debug('polUpdateBatch is at 3: '+ polUpdateBatch);
                Database.executeBatch(new POLBatchable(polUpdateBatch,polUpdates.size()), 1);
                polUpdateBatch.clear();
                requestCounter=requestCounter+batchSize;
                System.debug('requestCounter '+requestCounter);
            }
            
        }

    }
    @InvocableMethod(label='Purchase Order Line Batch Update' description='Sends Purchase Order Lines to be updated in batches.')
    public static void polUpdate(List<AcctSeedERP__Purchase_Order_Line__c> polUpdates)
    {
        System.debug('About to enqueue the job ' + polUpdates);
          ID jobID=System.enqueueJob(new PurchaseOrderLineQueueable(polUpdates));
          System.debug(jobID);
    }
}