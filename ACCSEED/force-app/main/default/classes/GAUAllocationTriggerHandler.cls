public class GAUAllocationTriggerHandler {
    
    public static void rollupFunds(List<npsp__Allocation__c> newAllocations, List<npsp__Allocation__c> oldAllocations, String recordType, Boolean isPaid){
        Set<Id> gauIds = new Set<Id>();
        Set<Id> campaignIds = new Set<Id>();

        
        Map<Id,npsp__Allocation__c> oldAllocationsMap = new Map<Id,npsp__Allocation__c>();
        
        RecordType rType = [SELECT id from RecordType where SobjectType='npsp__Allocation__c' and DeveloperName=:recordType];
        
        if(oldAllocations != null && !oldAllocations.isEmpty()){
            for(npsp__Allocation__c allocation : oldAllocations){
                if(rType.Id == allocation.RecordTypeId){
                    oldAllocationsMap.put(allocation.Id, allocation);     
                }
            }
        }
        
        npsp__Allocation__c oldAlloc;
        
        for(npsp__Allocation__c allocation : newAllocations){
            if(rType.Id == allocation.RecordTypeId){
                if(oldAllocationsMap.containsKey(allocation.Id)){
                    oldAlloc = oldAllocationsMap.get(allocation.Id);
                    if(oldAlloc.npsp__Amount__c != allocation.npsp__Amount__c || oldAlloc.Paid__c != allocation.Paid__c || oldAlloc.Total_Amount_Spent__c != allocation.Total_Amount_Spent__c ){
                        gauIds.add(allocation.npsp__General_Accounting_Unit__c);
                        campaignIds.add(allocation.npsp__Campaign__c);
                    }                     
                }else{
                    gauIds.add(allocation.npsp__General_Accounting_Unit__c);
                    campaignIds.add(allocation.npsp__Campaign__c);
                }   
            }
        }
        
        if(!gauIds.isEmpty()){
            Map<Id,npsp__Allocation__c> allocationsMap;
            if(isPaid){
                allocationsMap = new Map<Id,npsp__Allocation__c>([Select id, npsp__Amount__c, npsp__General_Accounting_Unit__c, Ledger_Disbursement_Amount__c, Total_Amount_Spent__c  from npsp__Allocation__c 
                                                                                          where npsp__General_Accounting_Unit__c in : gauIds and RecordTypeId=:rType.Id and Paid__c = True]);                
            }else{
                allocationsMap = new Map<Id,npsp__Allocation__c>([Select id, npsp__Amount__c, npsp__General_Accounting_Unit__c, Ledger_Disbursement_Amount__c, Total_Amount_Spent__c  from npsp__Allocation__c 
                                                                                          where npsp__General_Accounting_Unit__c in : gauIds and RecordTypeId=:rType.Id]);                
            }
    
            System.debug('allocationsMap'+allocationsMap);  
            
            Map<Id,npsp__General_Accounting_Unit__c> gauMap = new Map<Id,npsp__General_Accounting_Unit__c>([Select id, Total_Allocations1__c, Total_Disbursements__c,Total_Dispersements_Spent__c,Total_Paid_Allocations__c from npsp__General_Accounting_Unit__c where Id in : gauIds]);
            
            Map<Id,List<npsp__Allocation__c>> childGAUMap = new Map<Id,List<npsp__Allocation__c>>();
            
            List<npsp__Allocation__c> allocations;
            
            if(allocationsMap != null && !allocationsMap.isEmpty()){
                Set <Id> childAllocIds = allocationsMap.keySet();
                
                for(Id allocationId: childAllocIds){
                    allocations = new List<npsp__Allocation__c>();
                    oldAlloc = allocationsMap.get(allocationId);
                    if(childGAUMap.containsKey(oldAlloc.npsp__General_Accounting_Unit__c)){
                        allocations = childGAUMap.get(oldAlloc.npsp__General_Accounting_Unit__c);
                    }
                    allocations.add(oldAlloc);
                    childGAUMap.put(oldAlloc.npsp__General_Accounting_Unit__c,allocations);
                }                
            }

            
            //gauIds = childGAUMap.keySet();
            npsp__General_Accounting_Unit__c gauObj;
            
            for(Id gauId: gauIds){
                gauObj = gauMap.get(gauId);
                if(childGAUMap.containsKey(gauId)){
                    allocations = childGAUMap.get(gauId);
                    Decimal amount = 0.00;
                    Decimal amount_spent = 0.00;
                    for(npsp__Allocation__c allocation: allocations){

                        if(recordType == 'Allocation'){
                            if(allocation.npsp__Amount__c == null){
                               allocation.npsp__Amount__c = 0.00;
                            }
                            amount = amount + allocation.npsp__Amount__c; 
                        }else if(recordType == 'Disbursement'){
                            if(allocation.Ledger_Disbursement_Amount__c != null){
                                amount = amount + allocation.Ledger_Disbursement_Amount__c; 
                            } 
                            if(allocation.Total_Amount_Spent__c != null){
                               amount_spent = amount_spent + allocation.Total_Amount_Spent__c;
                            }
                        }
                    }                    
                    if(isPaid){
                       gauObj.Total_Paid_Allocations__c = amount; 
                    }else{
                        if(recordType == 'Allocation'){
                            gauObj.Total_Allocations1__c = amount;
                        }else if(recordType == 'Disbursement'){
                            gauObj.Total_Disbursements__c = -amount; //Must be set to negative since Ledger_Disbursement_Amount__c is generally a negative amount, except for Reverse Disbusements (type "GC Unused Balance")
                            gauObj.Total_Dispersements_Spent__c = amount_spent;
                        }                        
                    }                    
                    gauMap.put(gauId,gauObj);
                }else{
                    Decimal amount = 0.00;
                    Decimal amount_spent = 0.00;
                    if(isPaid){
                       gauObj.Total_Paid_Allocations__c = amount; 
                    }else{
                        if(recordType == 'Allocation'){
                            gauObj.Total_Allocations1__c = amount;
                        }else if(recordType == 'Disbursement'){
                            gauObj.Total_Disbursements__c = amount;
                            gauObj.Total_Dispersements_Spent__c = amount_spent;
                        }                        
                    }                    
                    gauMap.put(gauId,gauObj);                    
                }
            }
            if(!gauMap.isEmpty()){
                System.debug('Update GAUs:'+gauMap.values());
                update gauMap.values();
            }  
        }
        if(!campaignIds.isEmpty() && recordType == 'Disbursement'){
            Map<Id,npsp__Allocation__c> allocationsMap;
                 allocationsMap = new Map<Id,npsp__Allocation__c>([Select id, npsp__Amount__c, npsp__Campaign__c, Ledger_Disbursement_Amount__c, Total_Amount_Spent__c  from npsp__Allocation__c 
                                                                                          where npsp__Campaign__c in : campaignIds and RecordTypeId=:rType.Id]);                
    
            System.debug('allocationsMap'+allocationsMap);  
            
            Map<Id,Campaign> campaignMap = new Map<Id,Campaign>([Select id, BudgetedCost, Total_Disbursements__c,Total_Disbursements_Spent__c, Total_Disbursements_Regular__c from Campaign where Id in : campaignIds]);
            
            Map<Id,List<npsp__Allocation__c>> childCampaignMap = new Map<Id,List<npsp__Allocation__c>>();
            
            List<npsp__Allocation__c> allocations;
            
            if(allocationsMap != null && !allocationsMap.isEmpty()){
                Set <Id> childAllocIds = allocationsMap.keySet();
                
                for(Id allocationId: childAllocIds){
                    allocations = new List<npsp__Allocation__c>();
                    oldAlloc = allocationsMap.get(allocationId);
                    if(childCampaignMap.containsKey(oldAlloc.npsp__Campaign__c)){
                        allocations = childCampaignMap.get(oldAlloc.npsp__Campaign__c);
                    }
                    allocations.add(oldAlloc);
                    childCampaignMap.put(oldAlloc.npsp__Campaign__c,allocations);
                }                
            }

            
            //campaignIds = childCampaignMap.keySet();
            Campaign campaignObj;
            
            for(Id campaignId: campaignIds){
                campaignObj = campaignMap.get(campaignId);
                if(childCampaignMap.containsKey(campaignId)){
                    allocations = childCampaignMap.get(campaignId);
                    Decimal amount = 0.00;
                    Decimal amount_regular = 0.00;
                    Decimal amount_spent = 0.00;
                    for(npsp__Allocation__c allocation: allocations){
                        if(allocation.Ledger_Disbursement_Amount__c != null){
                            amount = amount + allocation.Ledger_Disbursement_Amount__c;
                            if (allocation.Ledger_Disbursement_Amount__c < 0){
                                amount_regular = amount_regular + allocation.Ledger_Disbursement_Amount__c;
                            }
                        }                      
                        if(allocation.Total_Amount_Spent__c != null){
                            amount_spent = amount_spent + allocation.Total_Amount_Spent__c;

                        }
                    }                    

                            campaignObj.Total_Disbursements__c = -amount;
                            campaignObj.BudgetedCost = campaignObj.Total_Disbursements__c;
                            campaignObj.Total_Disbursements_Regular__c = -amount_regular;
                            campaignObj.Total_Disbursements_Spent__c = amount_spent;
                    campaignMap.put(campaignId,campaignObj);
                }else{
                    Decimal amount = 0.00;
                    Decimal amount_spent = 0.00;

                            campaignObj.Total_Disbursements__c = amount;
                            campaignObj.BudgetedCost = campaignObj.Total_Disbursements__c;
                            campaignObj.Total_Disbursements_Spent__c = amount_spent;
                        
                    campaignMap.put(campaignId,campaignObj);                    
                }
            }
            if(!campaignMap.isEmpty()){
                System.debug('Update Campaigns:'+campaignMap.values());
                update campaignMap.values();
            }  
        }

    }          
}