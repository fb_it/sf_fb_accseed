public with sharing class ShipmentTriggerHandler {

    @future(Callout=true)
    public static void sendStatusUpdate(String newListJSON, String oldMapJSON) {
        List<AcctSeedERP__Shipment__c> newList = (List<AcctSeedERP__Shipment__c>) JSON.deserialize(newListJSON, List<AcctSeedERP__Shipment__c>.class);
        Map<id, AcctSeedERP__Shipment__c> oldMap;
        if(oldMapJSON!=null) {
            oldMap = (Map<id, AcctSeedERP__Shipment__c>) JSON.deserialize(oldMapJSON, Map<id, AcctSeedERP__Shipment__c>.class);
        }

        List<AcctSeedERP__Shipment__c> shipmentsToSend = new List<AcctSeedERP__Shipment__c>();
        Set<id> OppProductIds = new Set<id>();
        for(AcctSeedERP__Shipment__c ship:[Select id, Shipment_Complete__c, AcctSeedERP__Sales_Order__r.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c,(Select id,AcctSeedERP__Quantity_Shipped__c, AcctSeedERP__Sales_Order_Line__r.AcctSeedERP__Opportunity_Product_Id__c from AcctSeedERP__Shipment_Lines__r) from AcctSeedERP__Shipment__c where id IN: newList])
        {
            if(oldMap==null || (ship.Shipment_Complete__c && !oldMap.get(ship.id).Shipment_Complete__c && ship.AcctSeedERP__Sales_Order__r.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c != null))
            {
                shipmentsToSend.add(ship);
                for(AcctSeedERP__Shipment_Line__c line:ship.AcctSeedERP__Shipment_Lines__r) {
                    OppProductIds.add(line.AcctSeedERP__Sales_Order_Line__r.AcctSeedERP__Opportunity_Product_Id__c);
                }
            }
        }

        Map<id,OpportunityLineItem> oppLineMap = new Map<Id, OpportunityLineItem>([Select id, tnw_mage_basic__Magento_ID__c from OpportunityLineItem where ID in: OppProductIds]);
        if(!shipmentsToSend.isEmpty())
        {
            magento2.Port m = new magento2.Port();
            String sessionId = MagentoCalls.fetchSessionID();
            List<AcctSeedERP__Shipment__c> shipmentsToUpdate = new List<AcctSeedERP__Shipment__c>();
            for(AcctSeedERP__Shipment__c ship:shipmentsToSend)
            {
                magento2.orderItemIdQtyArray oiArr = new magento2.orderItemIdQtyArray();
                List<magento2.orderItemIdQty> arr = new List<magento2.orderItemIdQty>();
                for(AcctSeedERP__Shipment_Line__c shipLine:ship.AcctSeedERP__Shipment_Lines__r)
                {
                    if(oppLineMap.containsKey(shipLine.AcctSeedERP__Sales_Order_Line__r.AcctSeedERP__Opportunity_Product_Id__c) && oppLineMap.get(shipLine.AcctSeedERP__Sales_Order_Line__r.AcctSeedERP__Opportunity_Product_Id__c).tnw_mage_basic__Magento_ID__c!=null) {
                        magento2.orderItemIdQty item = new magento2.orderItemIdQty();
                        item.qty = shipLine.AcctSeedERP__Quantity_Shipped__c;
                        item.order_item_id = Integer.valueof(oppLineMap.get(shipLine.AcctSeedERP__Sales_Order_Line__r.AcctSeedERP__Opportunity_Product_Id__c).tnw_mage_basic__Magento_ID__c);
                        arr.add(item);
                    }
                }
                if(!arr.isEmpty()) {
                    oiArr.complexObjectArray = arr;
                    system.debug(oiArr);
                    String shipmentID = m.salesOrderShipmentCreate(sessionId, ship.AcctSeedERP__Sales_Order__r.AcctSeedERP__Opportunity__r.tnw_mage_basic__Magento_ID__c, oiArr, null, 1, 0);

                    ship.Magento_Shipment_ID__c = shipmentID;
                    shipmentsToUpdate.add(ship);
                }
                else{
                    //TODO - Create a Case?
                }
            }

            update shipmentsToUpdate;
        }

    }
}