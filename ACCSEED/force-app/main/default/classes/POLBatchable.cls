public without sharing class POLBatchable implements Database.Batchable<Integer> {
    public List<AcctSeedERP__Purchase_Order_Line__c> polUpdateBatch;
    public Integer allPOLsSize;
    public Decimal numberOfBatches;
    public Integer batchCounter =0;
    public Integer failureCounter=0;


    public POLBatchable(List<AcctSeedERP__Purchase_Order_Line__c> polUpdateBatch, Integer allPOLsSize) {
        this.polUpdateBatch=polUpdateBatch;
        this.allPOLsSize=allPOLsSize;
    }
    public POLBatchable(List<AcctSeedERP__Purchase_Order_Line__c> polUpdateBatch) {
        this.polUpdateBatch=polUpdateBatch;
    }

    public Iterable<Integer> start(Database.BatchableContext context) {
        Integer[] values = new Integer[0];
        numberOfBatches=allPOLsSize/polUpdateBatch.size();
        //System.debug('number of batchess is '+numberOfBatches+ ' '+allProductsSize+' '+ productUpdateBatch.size());

            if(Math.mod(allPOLsSize,polUpdateBatch.size())==0){
                System.debug('mod 0');
                while(values.size() < 25 ) values.add(values.size());
            }
            else{
                System.debug('not mod 0');
                while(values.size() < 4) values.add(values.size());
            }
        
        
        return values;
        
        //List<CatalogProductUpdateActions.CatalogProductUpdateRequest> productUpdateBatch= productUpdateBatch;
        //return productUpdateBatch;
        
    }

    //@TODO: Global Void Execute
    public void execute(Database.BatchableContext context, Integer[] values) {
        List<AcctSeedERP__Purchase_Order__c> poList= new List<AcctSeedERP__Purchase_Order__c>();
        for(AcctSeedERP__Purchase_Order_Line__c pol: polUpdateBatch){ 
            AcctSeedERP__Purchase_Order__c po=new AcctSeedERP__Purchase_Order__c(id=pol.AcctSeedERP__Purchase_Order__c);
            System.debug('doing the thing with Version '+po.Version__c); 
            if (po.Version__c!='Revision'||po.Version__c==null) {
                po.Version__c='Revision';  
                poList.add(po);    
            }
        }
        update poList;
    
    }

    public void finish(Database.BatchableContext context) {
    }
}