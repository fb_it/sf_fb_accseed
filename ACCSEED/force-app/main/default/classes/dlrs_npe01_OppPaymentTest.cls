/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_npe01_OppPaymentTest
{
    @IsTest(SeeAllData=true)
    private static void testTrigger()
    {
Test.startTest();
        dlrs.RollupService.testHandler(TestUtils.createPayment());
        Test.stopTest();
    }
}