/*
Created by Steve Reitz, on March 3, 2020. Sreitz@firstbook.org

*/
@isTest(seeAllData=true)

private class AccountTriggerHandlerTest {
    public static testmethod void AccountTriggerHandlerTest() {
        String accountName='testAcct is very Testy';

        //Testing AccountToGLAVTrigger if Strong Reporting Requirements
        Account acct = new Account(Name=accountName, Strong_Reporting_Requirements__c=TRUE);
        insert acct;
        Integer count=[SELECT count() FROM AcctSeed__Accounting_Variable__c WHERE Name=:acct.Name];
        System.assertEquals(1,count);

        //Testing AccountToGLAVTrigger if NOT Strong Reporting Requirements
        Account acctF = new Account(Name=accountName+'False', Strong_Reporting_Requirements__c=FALSE);
        insert acctF;
        Integer countF=[SELECT count() FROM AcctSeed__Accounting_Variable__c WHERE Name=:acctF.Name];
        System.assertEquals(0,countF);
    }

    @isTest
    private static void changePOLevel(){
        Test.startTest();
        //create an Account hierarchy
        AcctSeed__Accounting_Variable__c glAccountVar= TestUtils.createAccountingVariable('1');
        System.debug('glar id is '+glAccountVar.id);
        List<Account> insertHierarchy =new List<Account>();
        List<Account> updateHierarchy =new List<Account>();
        
        Account a = TestUtils.createVendor(glAccountVar.Id);
        System.debug('a id is '+a.id);
        insertHierarchy.add(a);
        Account b= TestUtils.createVendor(glAccountVar.Id);
        b.ParentId=a.Id;
        b.Top_Level_Parent__c=a.Id;
        updateHierarchy.add(b);
        Account c= TestUtils.createVendor(glAccountVar.Id);
        c.ParentId=b.Id;
        c.Top_Level_Parent__c=a.Id;
        updateHierarchy.add(c);
        Account d= TestUtils.createVendor(glAccountVar.Id);
        d.ParentId=b.Id;
        d.Top_Level_Parent__c=a.Id;
        updateHierarchy.add(d);
        update updateHierarchy;

        //create the Vendor Discounts
        List<Supplier_Discount__c> vd4a= new List<Supplier_Discount__c>();
        vd4a=TestUtils.createVendorDiscounts(a,5); 

        //activate the Flow
        b.Own_PO__c=TRUE;
        update b;
        
        //List<Supplier_Discount__c> vd4b= new List<Supplier_Discount__c>();
        //Integer vd4b;
        //vd4b=[SELECT Count() FROM Supplier_Discount__c WHERE Vendor__c=:b.id];

        //System.assertNotEquals(b.Top_Level_Parent__c, a.Id);
        //System.assertEquals(vd4b,5);
        Test.stopTest();
    }

    @isTest 
    private static void unChangePOLevel(){
        Test.startTest();
        AcctSeed__Accounting_Variable__c glAccountVar= TestUtils.createAccountingVariable('1');
        Account a= TestUtils.createVendor(glAccountVar.Id);

        List<Supplier_Discount__c> vd4a= new List<Supplier_Discount__c>();
        vd4a=TestUtils.createVendorDiscounts(a,5); 

        Account bb = TestUtils.createChildVendor(glAccountVar.id,a.id);
        List<Supplier_Discount__c> vd4bb= new List<Supplier_Discount__c>();
        vd4bb=[SELECT Name FROM Supplier_Discount__c WHERE Vendor__c=:bb.id];
        System.assertEquals(vd4bb.size(),vd4a.size());

        bb.Own_PO__c=false;
        update bb;

        List<Supplier_Discount__c> vd4bb2= new List<Supplier_Discount__c>();
        vd4bb2=[SELECT Name FROM Supplier_Discount__c WHERE Vendor__c=:bb.id];
        System.assertEquals(vd4bb2.size(),0);
        
        Test.stopTest();
    }
     
}