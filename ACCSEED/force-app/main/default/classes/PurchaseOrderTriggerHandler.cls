public with sharing class PurchaseOrderTriggerHandler {
    public PurchaseOrderTriggerHandler() {

    }

    public static void deletePOLines(List<AcctSeedERP__Purchase_Order__c> pos){
        //List<AcctSeedERP__Purchase_Order_Line__c> polsToDelete = ;
        List<AcctSeedERP__Purchase_Order_Line__c> polsToDelete = new List<AcctSeedERP__Purchase_Order_Line__c>();
       
      
        for(AcctSeedERP__Purchase_Order_Line__c pol:[SELECT Id FROM AcctSeedERP__Purchase_Order_Line__c WHERE AcctSeedERP__Purchase_Order__c IN :pos ]){
            polsToDelete.add(pol);
        }
        
        
        
        if (polsToDelete.size()>0){
            System.debug(polsToDelete);
            delete polsToDelete;
        }
        
        

    }
}