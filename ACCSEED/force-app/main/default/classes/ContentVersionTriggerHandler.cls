public without sharing class ContentVersionTriggerHandler {

    public static void EmailFiles(List<ContentVersion> newList)
    {
        Set<Id> contactIds = new Set<id>();
        Set<Id> accountIds = new Set<id>();
        Set<Id> cdIds = new Set<id>();
        Map<id,Set<id>> cvContactIds = new Map<id,Set<id>>();
        for(ContentVersion cv:newList)
        {
            contactIds.add(cv.Contact1ID__c);
            contactIds.add(cv.Contact2ID__c);
            accountIds.add(cv.AccountID__c);
            cdIds.add(cv.ContentDocumentId);
            cvContactIds.put(cv.id,new Set<id>{cv.Contact1ID__c,cv.Contact2ID__c});
        }

        Map<id,List<ContentDocumentLink>> cdCDLIds = new Map<id,List<ContentDocumentLink>>();
        for(ContentDocumentLink cdl:[Select id,ContentDocumentId,LinkedEntityId from ContentDocumentLink where (LinkedEntityId IN: contactIds OR LinkedEntityId IN: accountIds) AND ContentDocumentId IN:cdIds])
        {
            List<ContentDocumentLink> cdls = cdCDLIds.get(cdl.ContentDocumentId);
            if(cdls == null)
            {
                cdls = new List<ContentDocumentLink>();
            }

            cdls.add(cdl);
            cdCDLIds.put(cdl.ContentDocumentId,cdls);
        }

        Map<id,Contact> contactMap = new Map<id,Contact>([Select id,Email, AccountId from Contact where id IN:contactIds]);
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        List<ContentDocumentLink> cdls = new List<ContentDocumentLink>();
        for(ContentVersion cv : [Select id, ContentDocumentID, AccountID__c, VersionData,FileType, Title from ContentVersion where id IN: newList])
        {
            for(Id contactId:cvContactIds.get(cv.id)) {
                if(contactID!=null && contactMap.containsKey(contactID) && contactMap.get(contactID).Email!=null) {
                    Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
                    semail.setSubject('Test Email'); //TODO Email Subject
                    String[] sendTo = new String[]{contactMap.get(contactID).Email};
                    semail.setToAddresses(sendTo);
                    semail.setPlainTextBody('Please find the attached file'); //TODO Email Body

                    blob attachBody = cv.VersionData;
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName(cv.title+'.'+cv.FileType);
                    efa.setBody(attachBody);
                    semail.setEntityAttachments(new List<String>{cv.Id});

                    emails.add(semail);

                    Boolean cdlExists = false;

                    if(cdCDLIds.containsKey(cv.ContentDocumentId)) {
                        for (ContentDocumentLink cdl : cdCDLIds.get(cv.ContentDocumentId))
                        {
                            if(cdl.LinkedEntityId == contactId && cdl.ContentDocumentId == cv.ContentDocumentId)
                            {
                                cdlExists = true;
                            }
                        }
                    }

                    if(!cdlExists) {
                        ContentDocumentLink cdl = new ContentDocumentLink();
                        cdl.LinkedEntityId = contactId;
                        cdl.ContentDocumentId = cv.ContentDocumentId;
                        cdls.add(cdl);
                    }
                }
            }

            Boolean cdlExists = false;
            if(cdCDLIds.containsKey(cv.ContentDocumentId)) {
                for (ContentDocumentLink cdl : cdCDLIds.get(cv.ContentDocumentId))
                {
                    if(cv.AccountId__c!=null && cdl.LinkedEntityId == cv.AccountId__c && cdl.ContentDocumentId == cv.ContentDocumentId)
                    {
                        cdlExists = true;
                    }
                }
            }

            if(!cdlExists && cv.AccountId__c!=null) {
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.LinkedEntityId = cv.AccountId__c;
                cdl.ContentDocumentId = cv.ContentDocumentId;
                cdls.add(cdl);
            }

        }
        system.debug('emails -- ' + emails);
        if(!emails.isEmpty() && !Test.isRunningTest()) {
            Messaging.sendEmail(emails);
        }

        insert cdls;
    }
}