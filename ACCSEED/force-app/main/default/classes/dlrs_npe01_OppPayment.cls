@IsTest
public with sharing class dlrs_npe01_OppPayment {
    @IsTest(SeeAllData=true)
    private static void testTrigger()
    {
        Test.startTest();
        dlrs.RollupService.testHandler(TestUtils.createPayment());
        Test.stopTest();
    }
}