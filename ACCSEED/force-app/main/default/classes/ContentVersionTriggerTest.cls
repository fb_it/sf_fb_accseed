@isTest
private class ContentVersionTriggerTest {

    @isTest
    private static void testContentVersionTrigger(){

        Account acct = new Account ();
        acct.Name = 'Test Acct';
        acct.BillingStreet = '123 Main Street';
        acct.BillingCity = 'Beverly Hills';
        acct.BillingCountry = 'US';
        acct.BillingState = 'CA';
        acct.BillingPostalCode = '90210';

        insert acct;
        
        Contact con = new Contact();
        con.FirstName = 'TestFirst';
        con.LastName = 'TestLast';
        con.Email = 'test@test.net';
        con.AccountId=acct.Id;
        con.Magento_Organization__c = acct.id;

        insert con;


        ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true,
                Contact1ID__c = con.id
                
        );
        insert contentVersionInsert;

    }
}