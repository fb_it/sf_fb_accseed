@isTest(seeAllData=true)
private class ShipmentTriggerTest {

    @IsTest
    private static void testShipmentTrigger() {

        // insert GL Accounts
        List<AcctSeed__GL_Account__c> glAccounts = new List<AcctSeed__GL_Account__c>();
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '1000-Cash',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Balance Sheet',
                        AcctSeed__Bank__c = true,
                        AcctSeed__Sub_Type_1__c = 'Assets',
                        AcctSeed__Sub_Type_2__c = 'Cash'
                )
        );
        insert glAccounts;

        AcctSeed__GL_Account__c GL= new AcctSeed__GL_Account__c(name='123', AcctSeed__Type__c='Expense', AcctSeed__Sub_Type_1__c='Labor');
        insert GL;

        AcctSeed__GL_Account__c GL1= new AcctSeed__GL_Account__c(name='124', AcctSeed__Type__c='Balance Sheet',AcctSeed__Sub_Type_1__c='Assets');
        insert GL1;

        // insert Billing Formats
        List<AcctSeed__Billing_Format__c> billingFormats = new List<AcctSeed__Billing_Format__c>();
        billingFormats.add(
                new AcctSeed__Billing_Format__c(
                        Name = 'Default Billing Product',
                        AcctSeed__Visualforce_PDF_Page__c = 'BillingProductPDF',
                        AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                        AcctSeed__Type__c = 'Billing',
                        AcctSeed__Sort_Field__c = 'Name'
                )
        );

        billingFormats.add(
                new AcctSeed__Billing_Format__c(
                        Name = 'Default Billing Service',
                        AcctSeed__Visualforce_PDF_Page__c = 'BillingServicePDF',
                        AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                        AcctSeed__Type__c = 'Billing',
                        AcctSeed__Sort_Field__c = 'Name'
                )
        );

        billingFormats.add(
                new AcctSeed__Billing_Format__c (
                        Name = 'Billing Outstanding Statement',
                        AcctSeed__Visualforce_PDF_Page__c = 'BillingOutstandingStatementPDF',
                        AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                        AcctSeed__ReplyTo_Email__c = 'test3463464364646@gmail.com',
                        AcctSeed__Type__c = 'Outstanding Statement'
                )
        );

        billingFormats.add(
                new AcctSeed__Billing_Format__c (
                        Name = 'Billing Activity Statement',
                        AcctSeed__Visualforce_PDF_Page__c = 'BillingActivityStatementPDF',
                        AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                        AcctSeed__ReplyTo_Email__c = 'test3463464364646@gmail.com',
                        AcctSeed__Type__c = 'Activity Statement'
                )
        );

        billingFormats.add(
                new AcctSeed__Billing_Format__c(
                        Name = 'Default Purchase Order',
                        AcctSeed__Visualforce_PDF_Page__c = 'BillingServicePDF',
                        AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                        AcctSeed__Type__c = 'Purchase Order',
                        AcctSeed__Sort_Field__c = 'Name'
                )
        );

        billingFormats.add(
                new AcctSeed__Billing_Format__c(
                        Name = 'Default Packing Slip',
                        AcctSeed__Visualforce_PDF_Page__c = 'BillingServicePDF',
                        AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                        AcctSeed__Type__c = 'Packing Slip',
                        AcctSeed__Sort_Field__c = 'Name'
                )
        );

        insert billingFormats;

        // insert 1 Ledger record
        List<AcctSeed__Ledger__c> ledgers = new List<AcctSeed__Ledger__c>();

        ledgers.add(
                new AcctSeed__Ledger__c(
                        Name = 'Actual',
                        AcctSeed__Type__c = 'Transactional',
                        AcctSeed__Default_Bank_Account__c = glAccounts[0].Id,
                        AcctSeed__Default_Billing_Format__c = billingFormats[0].Id,
                        AcctSeed__Billing_Outstanding_Statement_Format__c = billingFormats[2].Id,
                        AcctSeed__Billing_Activity_Statement_Format__c = billingFormats[3].Id,
                        AcctSeed__Default_Purchase_Order_Format__c = billingFormats[4].Id,
                        AcctSeed__Default_Packing_Slip_Format__c = billingFormats[5].Id
                )
        );

        insert ledgers;
        // insert 1 Account
        Account acct = new Account(Name = 'Test');
        insert acct;

        // insert 1 Product
        Product2 prod = new Product2(
                Name = 'Sample',
                Manufacturers_Suggested_Retail_Price__c = 1,
                AcctSeedERP__Inventory_Asset__c = true,
                AcctSeed__Inventory_Product__c = true,
                AcctSeed__Expense_GL_Account__c = GL.id,
                AcctSeed__Revenue_GL_Account__c = GL1.id
        );
        insert prod;

        AcctSeedERP__Warehouse__c wh = new AcctSeedERP__Warehouse__c(Name = 'test wh');
        insert wh;

        List<AcctSeedERP__Location__c> locs = new List<AcctSeedERP__Location__c>();
        locs.add(new AcctSeedERP__Location__c(Name = 'Finished Goods', AcctSeedERP__Warehouse__c = wh.Id, Magento_Sync__c = true));
        locs.add(new AcctSeedERP__Location__c(Name = 'NVIS Main Office', AcctSeedERP__Warehouse__c = wh.Id, Magento_Sync__c = true));
        insert locs;

        AcctSeed__Billing_Format__c bf = new AcctSeed__Billing_Format__c();
        bf.AcctSeed__Type__c = 'Activity Statement';
        bf.AcctSeed__Visualforce_PDF_Page__c = 'BillingActivityStatementPDF';
        bf.AcctSeed__Default_Email_Template__c = 'Activity_Statement_Email_Template';

        insert bf;


        AcctSeedERP__Inventory_Balance__c bal1 = new AcctSeedERP__Inventory_Balance__c(AcctSeedERP__Warehouse__c = wh.Id, AcctSeedERP__Product__c = prod.id, AcctSeedERP__Location__c = locs[0].Id, AcctSeedERP__Ledger__c=ledgers[0].id);
        insert bal1;


        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('salesOrderShipmentCreate'));

        AcctSeedERP__Shipment__c ship = [Select id,Shipment_Complete__c, (select id,AcctSeedERP__Sales_Order_Line__r.AcctSeedERP__Opportunity_Product_Id__c from AcctSeedERP__Shipment_Lines__r) FROM AcctSeedERP__Shipment__c where Shipment_Complete__c = false AND ID IN (Select AcctSeedERP__Shipment__c from AcctSeedERP__Shipment_Line__c WHERE AcctSeedERP__Sales_Order_Line__r.AcctSeedERP__Opportunity_Product_Id__c !=null) limit 1];

        Set<String> oliIds = new Set<String>();
        for(AcctSeedERP__Shipment_Line__c line:ship.AcctSeedERP__Shipment_Lines__r)
        {
            oliIds.add(line.AcctSeedERP__Sales_Order_Line__r.AcctSeedERP__Opportunity_Product_Id__c);
        }

        Integer i=0;
        OpportunityLineItem[] olis = [Select id,tnw_mage_basic__Magento_ID__c from OpportunityLineItem where ID IN:oliIds];
        for(OpportunityLineItem oli :olis)
        {
            if(oli.tnw_mage_basic__Magento_ID__c == null)
            {
                oli.tnw_mage_basic__Magento_ID__c = '1234'+i;
            }
            oliIds.add(oli.id);
            i++;
            update oli;
        }

        ship.Shipment_Complete__c = true;
        update ship;

    }

}