@isTest
private class CatalogProductUpdateTest {

    @isTest
    private static void testCatalogProductUpdate(){
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('login'));
        
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdate'));

        CatalogProductUpdateActions.CatalogProductUpdateRequest prod = new CatalogProductUpdateActions.CatalogProductUpdateRequest();
		
        for(Integer i=0; i<5;i++){
            prod.ProductId='3288'+i;
            //prod.IdentifierType = 'id';
            prod.price = 12.34;
            prod.MSRP = 56.78;
        }

        
        
        List<CatalogProductUpdateActions.CatalogProductUpdateRequest> prodList= new List<CatalogProductUpdateActions.CatalogProductUpdateRequest>();
        prodList.add(prod);
        Test.startTest();
        CatalogProductUpdateActions.catalogProductUpdate(prodList);
        CatalogProductUpdateBatchable prods= new CatalogProductUpdateBatchable(prodList);
        Test.stopTest();
        
        //System.assertEquals(1,prodList.size());
        
        //CatalogProductUpdateActions.catalogProductUpdateBatch(new List<CatalogProductUpdateActions.CatalogProductUpdateRequest>{prod});
    }
}