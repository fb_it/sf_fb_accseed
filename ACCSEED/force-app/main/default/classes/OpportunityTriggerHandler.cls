public class OpportunityTriggerHandler {
    
    public static void rollupFunds(List<npsp__Allocation__c> newAllocations, List<npsp__Allocation__c> oldAllocations, String recordType){
        Set<Id> oppIds = new Set<Id>();
        
        Map<Id,npsp__Allocation__c> oldAllocationsMap = new Map<Id,npsp__Allocation__c>();
        
        RecordType rType = [SELECT id from RecordType where SobjectType='npsp__Allocation__c' and DeveloperName=:recordType];
        
        if(oldAllocations != null && !oldAllocations.isEmpty()){
            for(npsp__Allocation__c allocation : oldAllocations){
                if(rType.Id == allocation.RecordTypeId){
                    oldAllocationsMap.put(allocation.Id, allocation);     
                }
            }
        }
        
        npsp__Allocation__c oldAlloc;
        
        if(newAllocations != null && !newAllocations.isEmpty()){
            for(npsp__Allocation__c allocation : newAllocations){
                if(rType.Id == allocation.RecordTypeId){
                    if(oldAllocationsMap.containsKey(allocation.Id)){
                        oldAlloc = oldAllocationsMap.get(allocation.Id);
                        if(oldAlloc.npsp__Amount__c != allocation.npsp__Amount__c || oldAlloc.Paid__c != allocation.Paid__c){
                            if(allocation.npsp__Opportunity__c != null){
                                oppIds.add(allocation.npsp__Opportunity__c);
                            }                            
                        }                     
                    }else{
                        if(allocation.npsp__Opportunity__c != null){
                        	oppIds.add(allocation.npsp__Opportunity__c);    
                        }
                        
                    }   
                }
            }
        }
        
        if(!oppIds.isEmpty()){
            Map<Id,npsp__Allocation__c> allocationsMap;
            allocationsMap = new Map<Id,npsp__Allocation__c>([Select id, npsp__Amount__c, npsp__Opportunity__c  from npsp__Allocation__c 
                                                                                          where npsp__Opportunity__c in : oppIds and RecordTypeId=:rType.Id]);
    
            System.debug('allocationsMap'+allocationsMap);  
            
            Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([Select id, Total_Restricted_Amount__c from Opportunity where Id in : oppIds]);
            
            Map<Id,List<npsp__Allocation__c>> childOppMap = new Map<Id,List<npsp__Allocation__c>>();
            
            List<npsp__Allocation__c> allocations;
            
            if(allocationsMap != null && !allocationsMap.isEmpty()){
                Set <Id> childAllocIds = allocationsMap.keySet();
                
                for(Id allocationId: childAllocIds){
                    allocations = new List<npsp__Allocation__c>();
                    oldAlloc = allocationsMap.get(allocationId);
                    if(childOppMap.containsKey(oldAlloc.npsp__Opportunity__c)){
                        allocations = childOppMap.get(oldAlloc.npsp__Opportunity__c);
                    }
                    allocations.add(oldAlloc);
                    childOppMap.put(oldAlloc.npsp__Opportunity__c,allocations);
                }                
            }

            
            //gauIds = childGAUMap.keySet();
            Opportunity oppObj;
            
            for(Id oppId: oppIds){
                oppObj = oppMap.get(oppId);
                if(oppObj != null){
                    if(childOppMap.containsKey(oppId)){
                        allocations = childOppMap.get(oppId);
                        Decimal amount = 0.00;
                        for(npsp__Allocation__c allocation: allocations){
                            if(allocation.npsp__Amount__c == null){
                                allocation.npsp__Amount__c = 0.00;
                            }
                           amount = amount + allocation.npsp__Amount__c; 
                        }                    
                        oppObj.Total_Restricted_Amount__c = amount;                    
                    }else{
                        Decimal amount = 0.00;
                        oppObj.Total_Restricted_Amount__c = amount;                    
                    }
                    oppMap.put(oppId,oppObj);                    
                }
            }
            if(!oppMap.isEmpty()){
                System.debug('Update oPps:'+oppMap.values());
                update oppMap.values();
            }  
        }
    }          
}