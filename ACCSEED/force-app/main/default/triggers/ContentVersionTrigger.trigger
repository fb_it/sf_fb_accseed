trigger ContentVersionTrigger on ContentVersion (after insert) {

    if(trigger.isAfter && trigger.isInsert)
    {
        ContentVersionTriggerHandler.EmailFiles(trigger.new);
    }
}