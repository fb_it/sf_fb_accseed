trigger POIMTrigger on AcctSeedERP__Purchase_Order_Inventory_Movement__c (after insert, after delete) {

    if(trigger.isInsert)
    {
        POIMTriggerHandler.SendToMagento(JSON.serialize(trigger.new), false);
    }
    if(trigger.isDelete) { POIMTriggerHandler.SendToMagento(JSON.serialize(trigger.old), true);}
}