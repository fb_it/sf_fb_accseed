trigger SalesOrderTrigger on AcctSeedERP__Sales_Order__c (after update) {

    if(trigger.isAfter && trigger.isUpdate)
    {
        SalesOrderTriggerHandler.sendStatusUpdate(JSON.serialize(trigger.new),JSON.serialize(trigger.oldMap));
        SalesOrderTriggerHandler.createInvoice(JSON.serialize(trigger.new),JSON.serialize(trigger.oldMap));
    }
}