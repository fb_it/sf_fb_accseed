trigger PurchaseOrderTrigger on AcctSeedERP__Purchase_Order__c (before delete) {
    if(trigger.isBefore && trigger.isDelete){
        PurchaseOrderTriggerHandler.deletePOLines(trigger.old);
    }

}