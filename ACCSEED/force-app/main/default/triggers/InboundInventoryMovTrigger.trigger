trigger InboundInventoryMovTrigger on AcctSeedERP__Inbound_Inventory_Movement__c (after insert, after delete) {

    if(trigger.isInsert)
    {
        InboundInventoryMovTriggerHandler.SendToMagento(JSON.serialize(trigger.new), false);
    }
    if(trigger.isDelete){ InboundInventoryMovTriggerHandler.SendToMagento(JSON.serialize(trigger.old), true);}
}