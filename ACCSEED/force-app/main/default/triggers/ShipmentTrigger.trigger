trigger ShipmentTrigger on AcctSeedERP__Shipment__c (after update) {

    if(trigger.isAfter && trigger.isUpdate)
    {
        ShipmentTriggerHandler.sendStatusUpdate(JSON.serialize(trigger.new),JSON.serialize(trigger.oldmap));
    }
}