trigger PurchaseOrderLineTrigger on AcctSeedERP__Purchase_Order_Line__c (after insert, after update) {
        AcctSeedERP__Purchase_Order_Line__c pol=Trigger.new[0];

        String polPOId=pol.AcctSeedERP__Purchase_Order__c;

        if(trigger.isUpdate){
            AcctSeedERP__Purchase_Order_Line__c polOld=trigger.oldMap.get(pol.Id);
            PurchaseOrderLineTriggerHandler.updateVersionStatus(pol,polOld, polPOId);
        }
        else {
            PurchaseOrderLineTriggerHandler.setVersionStatus(pol, polPOId);
        }


    //PurchaseOrderLineTriggerHandler.changeVersionStatus(Trigger.new,trigger.old);
}